//
//  ReportVc.swift
//  ParkingSpotter
//
//  Created by Guneet Singh Lamba on 05/03/18.
//  Copyright © 2018 Guneet Singh Lamba. All rights reserved.
//

import UIKit

class ReportVc: UIViewController {

    @IBOutlet weak var ticketNumber: UILabel!
    @IBOutlet weak var Carname: UILabel!
    @IBOutlet weak var PlateNumber: UILabel!
    @IBOutlet weak var SpotNUmber: UILabel!
    @IBOutlet weak var lotNUmber: UILabel!
    @IBOutlet weak var city: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        ticketNumber.text = UserDefaults.standard.value(forKey: "ticketNumber") as? String
        SpotNUmber.text = UserDefaults.standard.value(forKey: "spotNumber") as? String
        lotNUmber.text = UserDefaults.standard.value(forKey: "LotNumber") as? String
        city.text = UserDefaults.standard.value(forKey: "City") as? String
        PlateNumber.text = UserDefaults.standard.value(forKey: "CarPlateNumber") as? String
        Carname.text = UserDefaults.standard.value(forKey: "CarName") as? String
        
        
        print(ticketNumber.text!)

    }
}
