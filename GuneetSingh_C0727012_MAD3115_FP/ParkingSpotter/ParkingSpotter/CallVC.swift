//
//  CallVC.swift
//  ParkingSpotter
//
//  Created by Guneet Singh Lamba on 06/03/18.
//  Copyright © 2018 Guneet Singh Lamba. All rights reserved.
//

import UIKit
import CallKit
import MessageUI
class CallVC: UIViewController {

    @IBOutlet weak var Text_outlet: UIButton!
    @IBOutlet weak var Email_Outlet: UIButton!
    @IBOutlet weak var Call_outlet: UIButton!
    
    @IBAction func Call_Action(_ sender: Any) {
        print("Calling....")
        let url = URL(string: "tel://+14387778300")
        
        if UIApplication.shared.canOpenURL(url!){
            
            if #available(iOS 10,*)
            {
                UIApplication.shared.open(url!)
            }
            else {
                UIApplication.shared.openURL(url!)
                
            }
            
        }
    }
    
    @IBAction func Email_Action(_ sender: Any) {
        print("sending email")
        
        
        if MFMailComposeViewController.canSendMail() {
            let emailPicker = MFMailComposeViewController()
            
            
            emailPicker.mailComposeDelegate = self
            emailPicker.setSubject("Test Email")
            emailPicker.setMessageBody("Hello, how are you", isHTML: true)
            self.present(emailPicker, animated: true, completion: nil)
        }
    }
    @IBAction func Text_Action(_ sender: Any) {
        print("Messaging")
        
        if
            
            MFMessageComposeViewController.canSendText() {
            let messagevc = MFMessageComposeViewController()
            messagevc.body = "Hello, how are you?"
            messagevc.recipients = ["+14387778300"]
            messagevc.messageComposeDelegate = self as? MFMessageComposeViewControllerDelegate
            self.present(messagevc, animated: true, completion: nil)
            
        }
    }
    override func viewDidLoad() {
        
        
        super.viewDidLoad()
}
}
extension CallVC: MFMailComposeViewControllerDelegate {
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: NSError?) {
        controller.dismiss(animated: true, completion: nil)
}
}
