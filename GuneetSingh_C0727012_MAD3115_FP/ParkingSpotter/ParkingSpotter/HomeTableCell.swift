//
//  HomeTableCell.swift
//  ParkingSpotter
//
//  Created by Guneet Singh Lamba on 23/02/18.
//  Copyright © 2018 Guneet Singh Lamba. All rights reserved.
//

import UIKit

class HomeTableCell: UITableViewCell {

    @IBOutlet weak var TicketNumber_Label: UILabel!
    @IBOutlet weak var Location_Label: UILabel!
    @IBOutlet weak var TicketNumber_Value: UILabel!
    @IBOutlet weak var Location_Value: UILabel!
    



}
