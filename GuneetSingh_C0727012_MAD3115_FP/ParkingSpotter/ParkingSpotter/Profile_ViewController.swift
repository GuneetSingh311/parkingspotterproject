//
//  Profile_ViewController.swift
//  ParkingSpotter
//
//  Created by Guneet Singh Lamba on 22/02/18.
//  Copyright © 2018 Guneet Singh Lamba. All rights reserved.
//

import UIKit

class Profile_ViewController: UIViewController {

    @IBOutlet weak var ProfileView: UIView!
    @IBOutlet weak var UserImage: UIImageView!
    
    @IBOutlet weak var NAme: UILabel!
    
    @IBOutlet weak var Age: UILabel!
    
    @IBOutlet weak var CarName: UILabel!
    
    @IBOutlet weak var Email_TextField: UITextField!
    
    @IBAction func UpdateEmail_Button(_ sender: Any) {
    Email_TextField.isHidden = false
    ok_button.isHidden = false
    
    }
    @IBOutlet weak var ok_button: UIButton!
    @IBOutlet weak var City: UILabel!
    @IBOutlet weak var CarPlateNumber: UILabel!
    
    @IBAction func Ok_Action(_ sender: Any) {
        
UserDefaults.standard.set(Email_TextField.text!, forKey: "Email")
        Email_TextField.isHidden = true
        ok_button.isHidden = true
    
    }
    @IBAction func SignOut_Action(_ sender: Any) {
        performSegue(withIdentifier: "to_main", sender: nil)
}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Email_TextField.isHidden = true
        ok_button.isHidden = true
        self.UserImage.layer.cornerRadius =  UserImage.frame.height / 2
        NAme.text = UserDefaults.standard.value(forKey: "Name") as? String
        City.text = UserDefaults.standard.value(forKey: "City") as? String
        CarName.text = UserDefaults.standard.value(forKey: "CarName") as? String
        CarPlateNumber.text = UserDefaults.standard.value(forKey:"CarPlateNumber") as? String
        let ImageData = UserDefaults.standard.object(forKey: "imageData") as! NSData
        UserImage.image = UIImage(data: ImageData as Data)
        ProfileView.layer.cornerRadius = 4.0
        ProfileView.addSubview(UserImage)
        let now = Date()
        let birthday: Date = UserDefaults.standard.value(forKey: "DateofBirth") as! Date
        let calendar = Calendar.current
        
        let ageComponents = calendar.dateComponents([.year], from: birthday, to: now)
        Age.text = String(ageComponents.year!)
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    @ objc func dismissKeyboard() {
        
        view.endEditing(true)
        Email_TextField.isHidden = true
        ok_button.isHidden = true
    }
        
}
