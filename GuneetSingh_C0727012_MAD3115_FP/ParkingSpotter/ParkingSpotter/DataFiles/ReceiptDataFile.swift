//
//  ReceiptDataFile.swift
//  ParkingSpotter
//
//  Created by Guneet Singh Lamba on 05/03/18.
//  Copyright © 2018 Guneet Singh Lamba. All rights reserved.
//

import Foundation
import UIKit

class ReceiptDataFile {
    
    var ticketNumber:String?
    var spotNumber:String?
    var LotNumber:String?
    var Address:String?
    var Area:String?
    var City:String?
    var ticketTime:String?
    var NumberOfHours:String?
    var TotalPrice:String?
    var carName:String

    init(ticketNumber:String,spotNumber:String,LotNumber:String,Address:String,Area:String,City:String,ticketTime:String,NumberOfHours:String,TotalPrice:String,CarName:String)
    {
        
        self.ticketNumber = ticketNumber
        self.spotNumber = spotNumber
        self.LotNumber = LotNumber
        self.Address = Address
        self.Area = Area
        self.City = City
        self.ticketTime = ticketTime
        self.NumberOfHours = NumberOfHours
        self.TotalPrice = TotalPrice
        self.carName = CarName
        
         UserDefaults.standard.set(self.ticketNumber, forKey: "ticketNumber")
         UserDefaults.standard.set(self.spotNumber, forKey: "spotNumber")
         UserDefaults.standard.set(self.LotNumber, forKey: "LotNumber")
         UserDefaults.standard.set(self.Address, forKey: "Address")
         UserDefaults.standard.set(self.Area, forKey: "Area")
         UserDefaults.standard.set(self.City, forKey: "City")
         UserDefaults.standard.set(self.ticketTime, forKey: "ticketTime")
         UserDefaults.standard.set(self.NumberOfHours, forKey: "NumberOfHours")
         UserDefaults.standard.set(self.TotalPrice, forKey: "TotalPrice")
         UserDefaults.standard.set(self.carName, forKey: "CarName")
    }
    
}
