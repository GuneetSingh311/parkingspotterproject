//
//  DataFile.swift
//  ParkingSpotter
//
//  Created by Guneet Singh Lamba on 27/02/18.
//  Copyright © 2018 Guneet Singh Lamba. All rights reserved.
//

import Foundation
import UIKit
class DataFile
{
    
    var Name:String?
    var Email:String?
    var Password:String?
    var CarPlateNumber:String?
    var DateofBirth:Date?
    var City:String?
    var UserImage:UIImage?

    init(Name:String,Email:String,Password:String,CarPlateNumber:String,DateofBirth:Date,City:String,UserImage:UIImage)
    {
        
        self.Name = Name
        self.Email = Email
        self.Password = Password
        self.CarPlateNumber = CarPlateNumber
        self.DateofBirth = DateofBirth
        UserDefaults.standard.set(self.Name, forKey: "Name")
        UserDefaults.standard.set(self.Email, forKey: "Email")
        UserDefaults.standard.set(self.Password, forKey: "Password")
        UserDefaults.standard.set(self.CarPlateNumber, forKey: "CarPlateNumber")
        UserDefaults.standard.set(self.DateofBirth, forKey: "DateofBirth")
        UserDefaults.standard.set(self.City, forKey: "City")
        self.UserImage = UserImage
        let imageData:NSData = UIImagePNGRepresentation(self.UserImage!)! as NSData

        UserDefaults.standard.set(imageData, forKey: "imageData")
    }
    
    
    
    

}
