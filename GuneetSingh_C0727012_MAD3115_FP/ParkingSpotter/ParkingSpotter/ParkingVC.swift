//
//  ParkingVC.swift
//  ParkingSpotter
//
//  Created by Guneet Singh Lamba on 27/02/18.
//  Copyright © 2018 Guneet Singh Lamba. All rights reserved.
//

import UIKit

class ParkingVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
  
    

    @IBOutlet weak var ParkingTable1: UITableView!
   
    
    var MenuOption1:[String] = ["Parking Payment Receipt","Parking Location","Parking Report","Parking Manual"]
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return MenuOption1.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Parking_cell1", for: indexPath)
        cell.textLabel?.text = MenuOption1[indexPath.section]
        cell.layer.borderWidth = 1
        cell.layer.cornerRadius = 8
        cell.clipsToBounds = true
        return cell
    }
   
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath)
        if cell?.textLabel?.text == "Parking Payment Receipt" {
            performSegue(withIdentifier: "Payment_Receipt", sender: nil)
        }
        else   if cell?.textLabel?.text == "Parking Location" {
            performSegue(withIdentifier: "Parking_Location", sender: nil)
        }
        else   if cell?.textLabel?.text == "Parking Report" {
            performSegue(withIdentifier: "Parking_Report", sender: nil)
        }
        else   if cell?.textLabel?.text == "Parking Manual" {
            performSegue(withIdentifier: "Parking_Manual", sender: nil)
        }

        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       return 80
  }
 
   
    
    override func viewDidAppear(_ animated: Bool) {
       // super.viewDidAppear(true)
         ParkingTable1.reloadSections([0,1,2,3], with: UITableViewRowAnimation.left)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        ParkingTable1.reloadData()
        ParkingTable1.delegate = self
        ParkingTable1.dataSource = self
       
       }
}
