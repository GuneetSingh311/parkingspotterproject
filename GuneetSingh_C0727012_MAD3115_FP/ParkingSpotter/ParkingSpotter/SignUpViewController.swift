//
//  SignUpViewController.swift
//  ParkingSpotter
//
//  Created by Guneet Singh Lamba on 21/02/18.
//  Copyright © 2018 Guneet Singh Lamba. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate {
    

    @IBOutlet weak var SignUpview: UIView!
    @IBOutlet weak var Name_SignUp: UITextField!
    @IBOutlet weak var Email_SignUp: UITextField!
    @IBOutlet weak var Password_SignUp: UITextField!
     @IBOutlet weak var ConfirmPassword_SignUp: UITextField!
    @IBOutlet weak var PhoneNumber_SignUp: UITextField!
    @IBOutlet weak var CarPlateNumber: UITextField!
    @IBOutlet weak var DatePicker_SignUp: UIDatePicker!
    @IBOutlet weak var NextButton_Outlet: UIButton!
    @IBOutlet weak var VehicleType_SignUp: UITextField!
    @IBOutlet weak var VehicleModel_SignUp: UITextField!
    @IBOutlet weak var CityPicker_Outlet: UIPickerView!
    @IBOutlet weak var SelectImage: UIImageView!
    
    var SelectedCityIndex:Int = 0
    var City:String?
    var cityList:[String] = ["Vancouver","Toronto","Ottawa","Calgary","Windsor","Edmenton","Brampton","Winnipeg"]

    @IBAction func Next_Action(_ sender: Any) {
       print("Running")
        displayvalues()
        
        DataFile(Name: Name_SignUp.text!, Email: Email_SignUp.text!, Password: Password_SignUp.text!, CarPlateNumber: CarPlateNumber.text!, DateofBirth: DatePicker_SignUp.date, City:City! , UserImage: SelectImage.image!)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return cityList.count
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        City = cityList[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
    return cityList[row]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CityPicker_Outlet.delegate = self
        CityPicker_Outlet.dataSource = self
        
        let TouchOnImage = UITapGestureRecognizer(target: self, action: #selector(PickImage))
        self.SelectImage.addGestureRecognizer(TouchOnImage)
        SelectImage.isUserInteractionEnabled = true
        SelectImage.layer.cornerRadius =  SelectImage.frame.height / 2
        SignUpview.layer.cornerRadius = 4.0
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
   
    @ objc func dismissKeyboard() {
        
        view.endEditing(true)
    }
  
    
    
    @objc func PickImage() {
        
        let ImagePicker = UIImagePickerController()
        ImagePicker.delegate = self
        ImagePicker.sourceType = .savedPhotosAlbum
        present(ImagePicker, animated: true, completion: nil)
        }
   
    
    
    func displayvalues() {
        self.SelectedCityIndex = self.CityPicker_Outlet.selectedRow(inComponent: 0)
        let allData:String = "\(self.Name_SignUp.text!) \n \(self.Email_SignUp.text!) \n\(self.DatePicker_SignUp.date) \n \(self.cityList[SelectedCityIndex])"
        let infoAlert = UIAlertController(title: "VerifyYourDetails", message: allData, preferredStyle: .actionSheet)
        infoAlert.addAction(UIAlertAction(title: "Confirm", style: .default, handler:{_ in self.proceed()}))
        self.present(infoAlert, animated: true, completion: nil)
    }
    
func proceed() {
    
        if Password_SignUp.text! == ConfirmPassword_SignUp.text! {
       self.performSegue(withIdentifier: "Home", sender: self)
            }
        else {
        ConfirmPassword_SignUp.backgroundColor = UIColor.red
            
            }
    }
}


extension SignUpViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            SelectImage.image = image
            
            dismiss(animated: true, completion: nil)
            
}
}
}
