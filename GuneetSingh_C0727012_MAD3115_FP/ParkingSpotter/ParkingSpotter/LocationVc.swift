//
//  LocationVc.swift
//  ParkingSpotter
//
//  Created by Guneet Singh Lamba on 01/03/18.
//  Copyright © 2018 Guneet Singh Lamba. All rights reserved.
//

import UIKit
import MapKit

class LocationVc: UIViewController {

    @IBOutlet weak var MapView: MKMapView!
    
    let regionRadius:CLLocationDistance = 100
    let LocationManager = CLLocationManager()
    var city:String?
    var sublocality:String?
    var address:String?

    override func viewDidLoad() {
        super.viewDidLoad()
        MapView.isZoomEnabled = true
        MapView.isScrollEnabled = true
        MapView.mapType = MKMapType.hybrid
        MapView.mapType = MKMapType.mutedStandard
        LocationManager.delegate = self
        LocationManager.desiredAccuracy = kCLLocationAccuracyBest
        LocationManager.requestAlwaysAuthorization()
        LocationManager.requestWhenInUseAuthorization()
        if  (CLLocationManager.locationServicesEnabled()) {
            
            LocationManager.startUpdatingLocation()
        }
        
    }
    
    func centreMapOnLocation(location:CLLocation,title:String,subtitle:String) {
                let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,regionRadius, regionRadius)
        MapView.setRegion(coordinateRegion, animated: true)
        
        // Put an annotation on the location
        let Annotation:MKPointAnnotation = MKPointAnnotation()
        Annotation.coordinate = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude)
        Annotation.title = title
        Annotation.subtitle = subtitle
        MapView.addAnnotation(Annotation)
    
    }
    
}

  extension LocationVc: CLLocationManagerDelegate {
    
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error.localizedDescription)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            LocationManager.requestLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location:CLLocation = locations[0]
        if locations.first != nil {
            print("location\(locations)")
        }
        centreMapOnLocation(location: LocationManager.location!, title: " My Location", subtitle: "")
    
        let geocoderlocation = CLGeocoder()
        geocoderlocation.reverseGeocodeLocation(location) { (placemarks, error) in
            
            let placemark = placemarks
            if  error == nil {
                
                self.city = placemark?.first?.locality
                self.sublocality = placemark?.first?.subLocality
                self.address = placemark?.first?.postalCode
                print(self.city!)
                print(self.sublocality!)
                print(self.address!)
                UserDefaults.standard.set(self.address, forKey: "address")
                UserDefaults.standard.set(self.sublocality, forKey: "sublocality")
                UserDefaults.standard.set(self.city, forKey: "city")
            }
                
            else{
                debugPrint(error!)
                
            }
}
   }
}
