//
//  Parking Receipt.swift
//  ParkingSpotter
//
//  Created by Guneet Singh Lamba on 27/02/18.
//  Copyright © 2018 Guneet Singh Lamba. All rights reserved.
//

import UIKit

class Parking_Receipt: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource {
   
    

    @IBOutlet weak var ReceiptView: UIView!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var LotNumber: UITextField!
    @IBOutlet weak var SpotNumber: UITextField!
    @IBOutlet weak var carplateNumber: UITextField!
    @IBOutlet weak var BrandPicker: UIPickerView!
    @IBOutlet weak var ParkingDatePicker: UIDatePicker!
    @IBOutlet weak var ProceedToButton_Outlet: UIButton!
    @IBOutlet weak var NumberOfHoursParked: UITextField!
    @IBOutlet weak var TotalPrice: UILabel!
    @IBOutlet weak var BrandLogoView: UIImageView!
    var carName:String? = nil
    var BrandName:[String] = ["ACURA","Aston Martin","Audi","Bentley","BMW","Bugati","Cadillac",
        "Chevrolet","Fiat","Ferrari","Ford","Jaguar",
        "Hyundai","Infinti","Jeep","LandRover","Mazda","Honda",
    "Lexus","Mercedes-Benz","RAM","Rolls Royce","Renault"]
    
    var BrandLogos:[UIImage] = [#imageLiteral(resourceName: "acura"),#imageLiteral(resourceName: "astonmartin"),#imageLiteral(resourceName: "audi"),#imageLiteral(resourceName: "bentley"),#imageLiteral(resourceName: "bmw"),#imageLiteral(resourceName: "bugati"),#imageLiteral(resourceName: "cadillac"),#imageLiteral(resourceName: "Chevorlet"),#imageLiteral(resourceName: "fiat"),#imageLiteral(resourceName: "ferrari"),#imageLiteral(resourceName: "ford"),#imageLiteral(resourceName: "jaguar"),#imageLiteral(resourceName: "hyundai"),#imageLiteral(resourceName: "infiniti"),#imageLiteral(resourceName: "jeep"),#imageLiteral(resourceName: "landrover"),#imageLiteral(resourceName: "mazda"),#imageLiteral(resourceName: "honda"),#imageLiteral(resourceName: "lexus"),#imageLiteral(resourceName: "mercedes-benz"),#imageLiteral(resourceName: "ram"),#imageLiteral(resourceName: "royce"),#imageLiteral(resourceName: "renault")]
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return BrandName.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
       carName = BrandName[row]
        UserDefaults.standard.set(carName, forKey: "CarName")
        return BrandName[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
       
        BrandLogoView.image = BrandLogos[row]
       }
    
    @IBAction func ProceedToButton_Action(_ sender: Any) {
        push()
    }
    func push()
    {
        if Int(NumberOfHoursParked.text!)! == 1
        {
            TotalPrice.text = "4$"
        }
        else if Int(NumberOfHoursParked.text!)! > 1
        {
            TotalPrice.text = String(Int(NumberOfHoursParked.text!)! * 4)
        }
        
        let infoalert = UIAlertController(title: "ProceedToPay", message: "\(TotalPrice.text!) $" , preferredStyle: .actionSheet)
        infoalert.addAction(UIAlertAction(title: "Confirm", style: .default, handler: { _ in self.proceed()}
        ))
          self.present(infoalert, animated: true, completion: nil)
        
       
        
    }
    func proceed() {
        let ticketNumber:String? = String(arc4random())
        let address:String? = UserDefaults.standard.value(forKey: "address") as? String
        let Area:String? = UserDefaults.standard.value(forKey: "sublocality") as? String
        let City:String? = UserDefaults.standard.value(forKey: "city") as? String
        let Pdate = String(describing: ParkingDatePicker.date)
        let MainStoryBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let FinalPayController = MainStoryBoard.instantiateViewController(withIdentifier: "FinalPay")
        self.navigationController?.pushViewController(FinalPayController, animated: true)
        
        ReceiptDataFile(ticketNumber: ticketNumber!, spotNumber: SpotNumber.text!, LotNumber: LotNumber.text!, Address: address!, Area: Area!, City: City!, ticketTime: Pdate, NumberOfHours: NumberOfHoursParked.text!, TotalPrice: TotalPrice.text!, CarName: carName! )
        }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.ReceiptView.layer.cornerRadius = 4.0
        BrandPicker.delegate = self
        BrandPicker.dataSource = self
        let Email = UserDefaults.standard.value(forKey: "Email")
        email.text = Email as? String
        let CarPlate = UserDefaults.standard.value(forKey: "CarPlateNumber")
        carplateNumber.text = CarPlate as? String
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
        }
    @ objc func dismissKeyboard() {
        
        view.endEditing(true)
    }
}
