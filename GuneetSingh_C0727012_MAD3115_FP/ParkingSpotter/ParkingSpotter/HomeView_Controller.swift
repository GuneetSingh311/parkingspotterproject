//
//  HomeView_Controller.swift
//  ParkingSpotter
//
//  Created by Guneet Singh Lamba on 23/02/18.
//  Copyright © 2018 Guneet Singh Lamba. All rights reserved.
//

import UIKit

var Tickets = [Dictionary<String,String>()]
class HomeView_Controller: UIViewController,UITableViewDelegate,UITableViewDataSource {
  
   
    
    @IBOutlet weak var HomeTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        HomeTable.delegate = self
        HomeTable.dataSource = self
        HomeTable.reloadData()
}
    
    override func viewDidAppear(_ animated: Bool) {
        
        if let tempPlaces = UserDefaults.standard.object(forKey: "tickets") as? [Dictionary<String,String>] {
            Tickets = tempPlaces
        }
        if Tickets.count == 0 && Tickets[0].count == 0 {
            Tickets.append(["TicketNumber":"No Ticket Issued","Location":"No Location Choosen"])
        }
        else {
            
            HomeTable.reloadData()
        }
        
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Tickets.count
    }
    
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! HomeTableCell
    cell.TicketNumber_Label.text = "TicketNo"
    cell.Location_Label.text = "Location"
    cell.TicketNumber_Value.text = Tickets[indexPath.row]["TicketNumber"]
    cell.Location_Value.text = Tickets[indexPath.row]["Location"]
    return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    performSegue(withIdentifier: "Ticket_description", sender: nil)
   }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
        
        
    }

}
