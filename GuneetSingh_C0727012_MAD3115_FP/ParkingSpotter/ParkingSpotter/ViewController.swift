//
//  ViewController.swift
//  ParkingSpotter
//
//  Created by Guneet Singh Lamba on 21/02/18.
//  Copyright © 2018 Guneet Singh Lamba. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
   let Email = UserDefaults.standard.value(forKey: "Email") as? String
    let password = UserDefaults.standard.value(forKey: "Password") as? String
    
    @IBOutlet weak var Email_SignIN: UITextField!
    @IBOutlet weak var Password_SignIN: UITextField!
    @IBOutlet weak var LogInButton_Outlet: UIButton!
    @IBOutlet weak var SwitchButton_Outlet: UISwitch!
    
    @IBAction func LoginButton_Action(_ sender: Any) {
      pass()
        }
    
    func pass() {
        if Password_SignIN.text != password {
            retry()
        }
        else {
        performSegue(withIdentifier: "Home", sender: nil)
    }
    }
    func retry() {
        Email_SignIN.text = "enter again"
        Password_SignIN.text = "enter again"
    }
    
   
   
    @IBAction func SwitchButton_Action(_ sender: Any) {
        if SwitchButton_Outlet.isOn == true {
        
          Email_SignIN.text = Email
          Password_SignIN.text = password
            
            }
         
        else {
          retry()
                }
           }
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        SwitchButton_Outlet.isOn = false
        SwitchButton_Outlet.thumbTintColor = UIColor.darkGray
       
        
    }

   }

