//
//  ParkingManualVC.swift
//  ParkingSpotter
//
//  Created by Guneet Singh Lamba on 04/03/18.
//  Copyright © 2018 Guneet Singh Lamba. All rights reserved.
//

import UIKit
import WebKit
class ParkingManualVC: UIViewController {

    @IBOutlet weak var WebView: WKWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
       
        loadPage()
    }
    func loadPage() {
        let localPath = Bundle.main.url(forResource: "Parking_Manual", withExtension: "html")
        let requestOBJ = NSURLRequest(url: localPath!)
        WebView.load(requestOBJ as URLRequest)
    }

 

}
