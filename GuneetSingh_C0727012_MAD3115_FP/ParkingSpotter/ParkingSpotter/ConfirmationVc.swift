//
//  ConfirmationVc.swift
//  ParkingSpotter
//
//  Created by Guneet Singh Lamba on 04/03/18.
//  Copyright © 2018 Guneet Singh Lamba. All rights reserved.
//

import UIKit

class ConfirmationVc: UIViewController {

    @IBOutlet weak var TicketNumber: UILabel!
    
    @IBOutlet weak var spotNumber: UILabel!
    
    @IBOutlet weak var lotNumber: UILabel!
    
    @IBOutlet weak var address: UILabel!
    
    @IBOutlet weak var area: UILabel!
    
    @IBOutlet weak var city: UILabel!
    
    @IBOutlet weak var ticketTime: UILabel!
    
    @IBOutlet weak var NumberOfHours: UILabel!
    
    @IBOutlet weak var TotalPrice: UILabel!

    

    @IBAction func Ok_Action(_ sender: Any) {

        Tickets.append(["TicketNumber":TicketNumber.text!,"Location":city.text!])
        UserDefaults.standard.set(Tickets, forKey: "tickets")
        Ticketinfo = "The ticket was Booked on \(ticketTime.text!)  at lot number \(lotNumber.text!) and spot number \(spotNumber.text!)\n The address at which ticket was booked was \(address.text!) near \(area.text!) in city \(city.text!) for \(NumberOfHours.text!) hours \n ticket fare was  \(TotalPrice.text!)."
        UserDefaults.standard.set(Ticketinfo, forKey: "ticketInfo")
}
    
    override func viewDidLoad()
    {
    super.viewDidLoad()
    TicketNumber.text = UserDefaults.standard.value(forKey: "ticketNumber") as? String
    spotNumber.text = UserDefaults.standard.value(forKey: "spotNumber") as? String
    lotNumber.text = UserDefaults.standard.value(forKey: "LotNumber") as? String
    address.text = UserDefaults.standard.value(forKey: "Address") as? String
    area.text = UserDefaults.standard.value(forKey: "Area") as? String
    city.text = UserDefaults.standard.value(forKey: "City") as? String
    ticketTime.text = UserDefaults.standard.value(forKey: "ticketTime")  as? String
    NumberOfHours.text = UserDefaults.standard.value(forKey: "NumberOfHours") as? String
    TotalPrice.text = "$ \(UserDefaults.standard.value(forKey: "TotalPrice")! as! String)"
    }
}
