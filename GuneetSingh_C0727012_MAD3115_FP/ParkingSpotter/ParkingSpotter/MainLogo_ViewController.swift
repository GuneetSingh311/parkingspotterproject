//
//  MainLogo_ViewController.swift
//  ParkingSpotter
//
//  Created by Guneet Singh Lamba on 21/02/18.
//  Copyright © 2018 Guneet Singh Lamba. All rights reserved.
//

import UIKit

class MainLogo_ViewController: UIViewController {

    @IBOutlet weak var Logo_Image: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

      Logo_Image.image = UIImage(named: "Logo_Image.png")
    
        Logo_Image.layer.cornerRadius = Logo_Image.frame.height / 2
    
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
