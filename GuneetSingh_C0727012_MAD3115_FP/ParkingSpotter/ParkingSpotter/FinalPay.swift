//
//  FinalPay.swift
//  ParkingSpotter
//
//  Created by Guneet Singh Lamba on 27/02/18.
//  Copyright © 2018 Guneet Singh Lamba. All rights reserved.
//

import UIKit

class FinalPay: UIViewController,UIPickerViewDataSource,UIPickerViewDelegate {
   
    

    @IBOutlet weak var SelectPaymentView:
    UIView!
    @IBOutlet weak var CardTypePicker: UIPickerView!
    
    @IBOutlet weak var CardDetailsView: UIView!
    @IBOutlet weak var CardHolderName: UITextField!
    
    @IBOutlet weak var CardNumber: UITextField!
    @IBOutlet weak var Month: UITextField!
    @IBOutlet weak var Year: UITextField!
    
    @IBOutlet weak var CVV: UITextField!
    
    @IBOutlet weak var PayButton_Outlet: UIButton!
    
    var cards:[String] = ["Visa","Debit","Master"]
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return cards.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return cards[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        CardDetailsView.isHidden = false
    }
    @IBAction func PayButton_Action(_ sender: Any) {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CardDetailsView.isHidden = true
        SelectPaymentView.layer.cornerRadius = 4.0
        CardDetailsView.layer.cornerRadius = 4.0
        CardTypePicker.delegate = self
        CardTypePicker.dataSource = self
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    @ objc func dismissKeyboard() {
        
        view.endEditing(true)
    }
}
