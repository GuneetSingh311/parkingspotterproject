//
//  SupportContactVc.swift
//  ParkingSpotter
//
//  Created by Guneet Singh Lamba on 06/03/18.
//  Copyright © 2018 Guneet Singh Lamba. All rights reserved.
//

import UIKit
import Contacts

struct myContacts {
    var givenName:String
    var familyName:String
    var phoneNo:String
    var emailId:String
    
}
class SupportContactVc: UIViewController,UITableViewDelegate,UITableViewDataSource {
  
    

    @IBOutlet weak var ContactsTable: UITableView!
    var myContactStore = CNContactStore()
    var myContactslist = [myContacts]()
    override func viewDidLoad() {
        super.viewDidLoad()
       
        ContactsTable.reloadData()
        ContactsTable.delegate = self
        ContactsTable.dataSource = self
        myContactStore.requestAccess(for: .contacts) { (success, error) in
            if success  {
                print("Authorization Succesfull")
            }
        }
        self.fetchContacts()
        
    }
    func fetchContacts()
    {
        let Key = [CNContactGivenNameKey,CNContactFamilyNameKey,CNContactPhoneNumbersKey,CNContactEmailAddressesKey] as [CNKeyDescriptor]
        
        
        let request = CNContactFetchRequest(keysToFetch: Key)
        try!myContactStore.enumerateContacts(with: request) {
            (contact, stoppingPointer)
            in
            print("Contact: \(contact)")
            
            let givenName = contact.givenName as String
            let familyName = contact.familyName as String
            var phoneNo = " "
            if (contact.phoneNumbers.isEmpty) {
                phoneNo = contact.phoneNumbers[0].value
                    .stringValue
            }
            
            var emailId = ""
            if(!contact.emailAddresses.isEmpty) {
                emailId = contact.emailAddresses[0].value as String
            }
            
            self.myContactslist.append(myContacts(givenName: givenName, familyName: familyName, phoneNo: phoneNo, emailId: emailId))
            self.ContactsTable.reloadData()
            
        }
    }

    func numberOfSections(in tableView: UITableView) -> Int {
      return  1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return myContactslist.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        cell.textLabel?.text = "\(myContactslist[indexPath.row].givenName) \(myContactslist[indexPath.row].familyName)"
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "to_Call", sender: nil)
    }
    
    
    }

    


