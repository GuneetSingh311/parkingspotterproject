//
//  DescriptionVc.swift
//  ParkingSpotter
//
//  Created by Guneet Singh Lamba on 06/03/18.
//  Copyright © 2018 Guneet Singh Lamba. All rights reserved.
//

import UIKit

var Ticketinfo:String?
class DescriptionVc: UIViewController {

    @IBOutlet weak var TicketDescription: UITextView!
    
    
    override func viewDidAppear(_ animated: Bool) {
        if let tempInfo = UserDefaults.standard.value(forKey: "ticketInfo") as? String {
            Ticketinfo = tempInfo
            }
        if Ticketinfo == " " {
            TicketDescription.text = "No tickets yet"
        }
        else {
            TicketDescription.text = Ticketinfo
        }
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }

  

}
