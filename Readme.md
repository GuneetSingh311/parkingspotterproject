#App which finds the nearby available parking spots and display it on map for user to locate it.
#App uses MapKit to get location and show nearby parking spots.
#Once the parking lots are availble user can select parking spot and buy a parking space for designated time.
#App uses tableViews and PickerViews for user to select options and display data.
#App gives Customer service option to help the user.
#App uses webView which direct to webapages explaining parking laws in specific areas.
#App uses view to display confirmation of receipt.
![Alt text](https://bitbucket.org/GuneetSingh311/parkingspotterproject/downloads/1.png)
![Alt text](https://bitbucket.org/GuneetSingh311/parkingspotterproject/downloads/2.png)
![Alt text](https://bitbucket.org/GuneetSingh311/parkingspotterproject/downloads/3.png)
![Alt text](https://bitbucket.org/GuneetSingh311/parkingspotterproject/downloads/4.png)
![Alt text](https://bitbucket.org/GuneetSingh311/parkingspotterproject/downloads/5.png)
![Alt text](https://bitbucket.org/GuneetSingh311/parkingspotterproject/downloads/6.png)
![Alt text](https://bitbucket.org/GuneetSingh311/parkingspotterproject/downloads/7.png)
![Alt text](https://bitbucket.org/GuneetSingh311/parkingspotterproject/downloads/8.png)
